package config

import (
	"fmt"
	"log"

	"github.com/fsnotify/fsnotify"
	"github.com/go-redis/redis/v9"

	"github.com/spf13/viper"
)


type Config struct {
	Constants
	Rdb *redis.Client


}
type Constants struct {
	PORT string
	Redis struct {
		Addr string
		Password string
		Db int

	}

}
func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addr,
		Password: config.Redis.Password,
		DB:       config.Redis.Db,
	})
	config.Rdb = rdb

	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("config.conf") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                    // Search the root directory for the configuration file
	err := viper.ReadInConfig()                 // Find and read the config file
	if err != nil {                             // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}
	var constants Constants

	err = viper.Unmarshal(&constants)
	return constants, err
}