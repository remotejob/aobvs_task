package models

type Hacker struct {

	Name string `json:"name"`
	Score int64 `json:"score"`
}
