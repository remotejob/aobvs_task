ref: for Мария Бащук (Специалист по координации работ)

# Task can be tested on server ip:206.81.3.58

curl -v http://206.81.3.58:8010/json/hackers |json_pp

 Content-Type: application/json; charset=utf-8 (as was requested)

Result:
```yaml
[
  {
    "name": "Alan Turing",
    "score": 1912
  },
  {
    "name": "Claude Shannon",
    "score": 1916
  },
  {
    "name": "Alan Kay",
    "score": 1940
  },
  {
    "name": "Richard Stallman",
    "score": 1953
  },
  {
    "name": "Yukihiro Matsumoto",
    "score": 1965
  },
  {
    "name": "Linus Torvalds",
    "score": 1969
  }
]


rest of my presonal projects locate on:
https://mazurov.eu
https://github.com/sinelga
https://github.com/remotejob
https://gitlab.com/remotejob (all in branches !!)


## Original task
разработчик (Go)
Задание: сделать приложение на Golang, запускающее web-сервер на порту 8010 и отвечающее на GET запросы /json/hackers присылая список хакеров из sorted set в Redis
Детали реализации:
На основе библиотеки fiber (https://gofiber.io/)
Ответ сервера должен включать заголовок Content-Type: "application/json"
Код должен эффективно использовать ресурсы ОС под нагрузкой 10k RPS
Конфигурация Redis:
redis 127.0.0.1:6379> zadd hackers 1953 "Richard Stallman"  
(integer) 1                                                 
redis 127.0.0.1:6379> zadd hackers 1940 "Alan Kay"          
(integer) 1                                                 
redis 127.0.0.1:6379> zadd hackers 1965 "Yukihiro Matsumoto"
(integer) 1                                                 
redis 127.0.0.1:6379> zadd hackers 1916 "Claude Shannon"    
(integer) 1                                                 
redis 127.0.0.1:6379> zadd hackers 1969 "Linus Torvalds"    
(integer) 1                                                 
redis 127.0.0.1:6379> zadd hackers 1912 "Alan Turing"
(integer) 1
JavaScript
Запрос к серверу:
curl -s http://localhost:8010/json/hackers |json_pp
Shell
Ожидаемый результат:
[
  {
    "name": "Alan Turing",
    "score": 1912
  },
  {
    "name": "Claude Shannon",
    "score": 1916
  },
  {
    "name": "Alan Kay",
    "score": 1940
  },
  {
    "name": "Richard Stallman",
    "score": 1953
  },
  {
    "name": "Yukihiro Matsumoto",
    "score": 1965
  },
  {
    "name": "Linus Torvalds",
    "score": 1969
  }
]