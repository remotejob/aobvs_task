package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/go-redis/redis/v9"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/remotejob/aobvs_task/internal/config"
	"gitlab.com/remotejob/aobvs_task/internal/models"
)

var (
	conf *config.Config
	err  error
)

func init() {

	conf, err = config.New()
	if err != nil {
		log.Fatalln(err)
	}

}

func main() {
	app := fiber.New()

	ctx := context.Background()

	app.Get("/json/hackers", func(c *fiber.Ctx) error {

		val, err := conf.Rdb.ZRangeWithScores(ctx, "hackers", 0, -1).Result()
		if err != nil {
			if err == redis.Nil {
				fmt.Println("key does not exists")
				return c.SendStatus(404)
			}
			panic(err)
		}

		var hackers []models.Hacker
		for _, v := range val {
			hacker := models.Hacker{v.Member.(string), int64(v.Score)}
			hackers = append(hackers, hacker)
		}

		hak, err := json.Marshal(hackers)
		if err != nil {
			panic(err)
		}

		c.Set("Content-type", "application/json; charset=utf-8")

		return c.SendString(string(hak))
	})

	log.Fatal(app.Listen(conf.PORT))
}
